const fs = require('fs');
const tinyreq = require('tinyreq');
const path = require('path');
const createBranchAndCommitChanges = require('./gitLabConnect');

// FONT JSON
let rawdata = fs.readFileSync('listOfFontsToDownload.json');
let allFonts = JSON.parse(rawdata);
const ALL_CHARSETS = ['latin', 'latin-ext', 'cyrillic', 'cyrillic-ext', 'greek', 'greek-ext', 'vietnamese'];

const createArrayFromString = (str) => {
	return str.split('\n');
}

const countLeadingWhitespace = (str) => {
	let count = 0;
	for (let i = 0; i < str.length; i++) {
		if (str[i] === ' ') {
			count++;
		} else {
			break;
		}
	}
	return count;
}

const removeSpaces = (str) => {
	return str.replace(/\s/g, '');
}

const countTabs = (str) => {
	return countLeadingWhitespace(str) / 2;
};

const trimNonLetters = (str) => {
	return str.replace(/[^a-zA-Z]/g, '');
}

const trimForCharset = (str) => {
	return str.replace(/^[^a-zA-Z]+|[^a-zA-Z]+$/g, '').replace(/\([^)]*\)/g, '');
}

const isSecondTypeOfFont = (str) => {
	return str.includes(': (');
}

const trimNonNumericChars = (str) => {
	return str.replace(/[^0-9]/g, '');
}

const removeCommasAndQuotes = (str) => {
	return str.replace(/[,']/g, '');
}

const extractRange = (str) => {
	return str.replace(/'/g, '').replace(/,\s*$/, '');
}

const removeSemicolons = (str) => {
	return str.replace(/;/g, '');
}

const removeFontSubstring = (str) => {
	return str.replace('https://fonts.gstatic.com/', '');
}

const manipulateString = (str) => {
	return str.slice(0, 4) + str.slice(-4, -1);
}

const cleanUrl = (str) => {
	const cleaned = str.replace(/url\(|\)/g, '');
	return cleaned.split(/\s+/)[0];
}

const getFolderPrefix = (str) => {
	switch (str) {
		case 'cyrillic':
			return 'cy';
		case 'cyrillic-ext':
			return 'cy';
		case 'greek':
			return 'el';
		case 'greek-ext':
			return 'el';
		case 'vietnamese':
			return 'vi';
		default:
			return 'la';
	}
};

const deleteFolderRecursive = (folderPath) => {
	if (fs.existsSync(folderPath)) {
		fs.readdirSync(folderPath).forEach((file) => {
			const curPath = path.join(folderPath, file);
			if (fs.lstatSync(curPath).isDirectory()) {
				deleteFolderRecursive(curPath);
			} else {
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(folderPath);
	}
}

const createObjectFromSassFile = (sassFile) => {
	const sassFileContent = fs.readFileSync(sassFile, 'utf8');
	const sassFileContentSplit = createArrayFromString(sassFileContent);

	let sassMapObject = {};
	let currentFontFamily = '';
	let currentLoopCharset = '';
	let currentBottomLevel = '';
	sassFileContentSplit.map((line) => {
		const currentTabCount = countTabs(line);

		// if the tab is 1, then we are at the top level - font family
		if (currentTabCount === 1) {
			const currentFont = trimNonLetters(line);
			if (currentFont) {
				currentFontFamily = currentFont;
				sassMapObject[currentFont] = {};
			}
		}

		// if the tab is 2, then we are at the second level - charset
		if (currentTabCount === 2) {
			const currentCharset = trimForCharset(line);
			if (currentCharset && currentFontFamily) {
				currentLoopCharset = currentCharset;
				sassMapObject[currentFontFamily][currentCharset] = {};
			}
		}

		// if the tab is 3, then we are at the third level - properties
		if (currentTabCount === 3) {
			// Second type of font
			if (isSecondTypeOfFont(line)) {
				currentBottomLevel = trimForCharset(line);
				sassMapObject[currentFontFamily][currentLoopCharset][currentBottomLevel] = {};
			} else {
				// format - 'font-weight': '300 800',
				const splitChars = line.split(': ');
				if (currentFontFamily && currentLoopCharset) {
					if (Object.keys(splitChars).length == 2) {
						sassMapObject[currentFontFamily][currentLoopCharset][trimForCharset(splitChars[0])] = extractRange(splitChars[1]);
					}
				}
			}
		}

		// if the tab is 4, then we are at the fourth level - properties
		if (currentTabCount === 4) {
			const splitChars = line.split(': ');
			if (currentFontFamily && currentLoopCharset && currentBottomLevel) {
				if (Object.keys(splitChars).length == 2) {
					sassMapObject[currentFontFamily][currentLoopCharset][currentBottomLevel][trimNonNumericChars(splitChars[0])] = extractRange(splitChars[1]);
				}
			}
		}
	});
	return sassMapObject;
};

const downloadFile = (url, fontName, fileName) => {
	const USER_AGENT = 'User-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36';


	tinyreq({
		url: `https://fonts.gstatic.com/${url}`,
		headers: {
			'user-agent': USER_AGENT,
		},
		encoding: null,
	})
		.then((body) => {

			const dirPath = __dirname + '/fonts/' + fontName;
			fs.mkdirSync(dirPath, { recursive: true });

			fs.writeFileSync(__dirname + '/fonts/' + fontName + '/' + fileName + '.woff2', body, {
				encoding: null,
				flag: 'w',
			});
		})
		.catch((err) => {
			console.log(err);
		});
};

function extractText(str) {
	const start = str.indexOf('/*') + 2;
	const end = str.indexOf('*/');
	return str.substring(start, end).trim();
}

function convertFontFaceStringToObject(fontFaceStr) {
	// Remove @font-face and curly braces from the string
	const strWithoutAtAndBraces = fontFaceStr.replace(/@font-face\s*{\s*|\s*}/g, '');
	// Split the string into an array of property-value pairs
	const propertyValuePairs = strWithoutAtAndBraces.split(';');
	// Remove any empty elements from the array
	const nonEmptyPairs = propertyValuePairs.filter((pair) => pair.trim() !== '');
	// Convert the array of pairs into an object
	const fontFaceObj = nonEmptyPairs.reduce((obj, pair) => {
		const [property, value] = pair.split(':');
		obj[property.trim()] = value.trim();
		return obj;
	}, {});
	return fontFaceObj;
}

const getObjFromGoogle = (fontName, url) => {
	const USER_AGENT = 'User-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36';
	let objOfGoogleFonts = {};

	console.log(`https://fonts.googleapis.com/css2?family=${url}`);

	return new Promise((resolve, reject) => {
		tinyreq({
			url: `https://fonts.googleapis.com/css2?family=${url}`,
			headers: {
				'user-agent': USER_AGENT,
			},
			encoding: 'utf8',
		})
			.then((body) => {
				const test = createArrayFromString(body);

				let currentCharset = '';
				const tempObj = {};
				test.map((item) => {
					if (item.includes(': ')) {
						const fontProperties = item.split(': ');
						if (fontProperties.length == 2) {
							const propName = removeSpaces(fontProperties[0]);
							let propValue = removeSemicolons(fontProperties[1]);

							switch (propName) {
								case 'font-family':
									break;
								case 'src':
									let temp = removeCommasAndQuotes(propValue);

									temp = cleanUrl(propValue);
									temp = removeFontSubstring(temp);

									tempObj['src'] = temp;
									break;
								case 'unicode-range':
									tempObj['unicode-range'] = propValue;
									break;
								default:
									tempObj[propName] = removeCommasAndQuotes(propValue);
									break;
							}

							if (propName === 'unicode-range') {
								let weightData = { [tempObj['font-weight']]: tempObj['src'] };
								objOfGoogleFonts[currentCharset]['unicode-range'] = tempObj['unicode-range'];

								if (!objOfGoogleFonts[currentCharset][tempObj['font-style']]) {
									objOfGoogleFonts[currentCharset][tempObj['font-style']] = {};
								}

								objOfGoogleFonts[currentCharset][tempObj['font-style']][tempObj['font-weight']] = tempObj['src'];
								for (let key in tempObj) {
									delete tempObj[key];
								}
							}
						}
					} else if (item.includes('/*')) {
						currentCharset = extractText(item);
						if (!objOfGoogleFonts[currentCharset]) {
							objOfGoogleFonts[currentCharset] = [];
						}
					}
				});
				resolve(objOfGoogleFonts);
			})
			.catch((err) => {
				console.log(err);
			});
	});
};

const checkIfFontsAreEqual = (fontName, fontFromGoogle, fontsFromJson) => {
	const fontNameWOspaces = removeSpaces(fontName);
	let returnObj = {};
	if (Object.keys(fontsFromJson).length === 0) {
		ALL_CHARSETS.map((charset) => {
			let fontWeight = fontFromGoogle[charset]['font-weight'];
			const fontFolderName = `${getFolderPrefix(charset)}-${fontName.replace(/ /g, '-').toLowerCase()}`;

			if (!fontWeight) {
				Object.keys(fontFromGoogle[charset]['normal']).map((fontW) => {
					const fileName = `${fontFolderName}-${charset}-normal-${fontW}`;
					downloadFile(fontFromGoogle[charset]['normal'][fontW], fontFolderName, fileName);
				});

				if (fontFromGoogle[charset]['italic']) {
					Object.keys(fontFromGoogle[charset]['italic']).map((fontW) => {
						const fileName = `${fontFolderName}-${charset}-italic-${fontW}`;

						downloadFile(fontFromGoogle[charset]['italic'], fontFolderName, fileName);
					});
				}
			} else {
				// font weight is in short form
				fontWeight = fontWeight.replace(' ', '-');
				const fileName = `${fontFolderName}-${charset}-normal-${fontWeight}`;
				const fileNameItalic = `${fontFolderName}-${charset}-italic-${fontWeight}`;
				downloadFile(fontFromGoogle[charset]['normal'], fontFolderName, fileName);
				downloadFile(fontFromGoogle[charset]['italic'], fontFolderName, fileNameItalic);
			}

			returnObj[charset] = fontFromGoogle[charset];
		});

		return fontFromGoogle;
	}

	ALL_CHARSETS.map((charset) => {
		if (fontFromGoogle[charset]['normal'] === fontsFromJson[fontNameWOspaces][charset]['normal']) {
			normalIsTheSame = true;
		}

		if (fontFromGoogle[charset]['italic'] === fontsFromJson[fontNameWOspaces][charset]['italic']) {
			italicIsTheSame = true;
		}
	});
}

function mergeFontsWithTheSameUrl(fontFromGoogle) {
	let formatedGoogleFonts = {};

	Object.keys(fontFromGoogle).map((font) => {
		let hasTheSameSrc = false;
		let normalFontWeightMerged = '';
		let firstSrc = '';
		let firstItalicSrc = '';

		if (fontFromGoogle[font]['normal']) {
			let i = 0;
			Object.keys(fontFromGoogle[font]['normal']).map((item) => {
				normalFontWeightMerged += `${item} `;
				if (i === 0) {
					firstSrc = fontFromGoogle[font]['normal'][item];
				} else {
					if (firstSrc === fontFromGoogle[font]['normal'][item]) {
						hasTheSameSrc = true;
					}
				}
				i++;
			});
		}

		if (fontFromGoogle[font]['italic']) {
			let i = 0;
			Object.keys(fontFromGoogle[font]['italic']).map((item) => {
				if (i === 0) {
					firstItalicSrc = fontFromGoogle[font]['italic'][item];
				}
			});
		}

		if (hasTheSameSrc) {
			formatedGoogleFonts[font] = {
				'font-weight': manipulateString(normalFontWeightMerged),
				'unicode-range': fontFromGoogle[font]['unicode-range'],
				normal: firstSrc,
				italic: firstItalicSrc,
			};
		} else {
			formatedGoogleFonts[font] = fontFromGoogle[font];
		}
	});

	return formatedGoogleFonts;
}

function saveObjectAsScss(map) {
	let mapString = `$allGoogleFonts: (\n`;
	Object.keys(map).map((key) => {
		mapString += `\t'${key.replace('-', '')}': (\n`;

		Object.keys(map[key]).map((charset) => {
			mapString += `\t\t'${charset}': (\n`;
			mapString += `\t\t\t'unicode-range': '${map[key][charset]['unicode-range']}',\n`;

			if (map[key][charset]['font-weight']) {
				mapString += `\t\t\t'font-weight': '${map[key][charset]['font-weight']}',\n`;
				mapString += `\t\t\t'normal': '${map[key][charset]['normal']}',\n`;
				mapString += `\t\t\t'italic': '${map[key][charset]['italic']}',\n`;
			} else {
				mapString += `\t\t\t'normal': (\n`;
				Object.keys(map[key][charset]['normal']).map((fontW) => {
					mapString += `\t\t\t\t'${fontW}': '${map[key][charset]['normal'][fontW]}',\n`;
				});
				mapString += `\t\t\t),\n`;
				mapString += `\t\t\t'italic': (\n`;
				if (map[key][charset]['italic']) {
					Object.keys(map[key][charset]['italic']).map((fontW) => {
						mapString += `\t\t\t\t'${fontW}': '${map[key][charset]['italic'][fontW]}',\n`;
					});
				}
				mapString += `\t\t\t),\n`;
			}
			mapString += `\t\t),\n`;
		});

		mapString += `\t),\n`;
	});
	mapString += ');';

	fs.writeFile('my-map.scss', mapString, { flag: 'w' }, function (err) {
		if (err) {
			console.error(err);
		} else {
			console.log('File has been created');
		}
	});
}

const getCurrentDateTime = () => {
	const now = new Date();
	const day = now.getDate().toString().padStart(2, '0');
	const month = (now.getMonth() + 1).toString().padStart(2, '0');
	const year = now.getFullYear().toString();
	const hours = now.getHours().toString().padStart(2, '0');
	const minutes = now.getMinutes().toString().padStart(2, '0');
	return `${day}-${month}-${year}_${hours}-${minutes}`;
}


// Create object from sass file
const objFromSassMap = createObjectFromSassFile('fonts.scss');

(async () => {
	let finalMap = {};
	let completedCount = 0;
	const totalCount = Object.keys(allFonts).length;
	deleteFolderRecursive('fonts');

	for (const font of Object.keys(allFonts)) {
		const allFontWeights = allFonts[font];

		try {
			const response = await getObjFromGoogle(font, allFonts[font]);
			const updatedGoogleFonts = mergeFontsWithTheSameUrl(response);
			let whatToWriteToScssMap = {};
			whatToWriteToScssMap[font.replace(/ /g, '-')] = checkIfFontsAreEqual(font, updatedGoogleFonts, objFromSassMap);

			finalMap = { ...finalMap, ...whatToWriteToScssMap };
		} catch (error) {
			console.error(error);
		} finally {
			completedCount++;
			if (completedCount === totalCount) {
				saveObjectAsScss(finalMap);

				// COMMIT CHANGES
				setTimeout(() => {
					createBranchAndCommitChanges(`BOT-google-font-update-${getCurrentDateTime()}`, `BOT Google font update: ${getCurrentDateTime()}`, `BOT Google font update MR: ${getCurrentDateTime()}`);
				}, 15000);
			}
		}
	}
})();