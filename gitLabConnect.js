const { execSync } = require('child_process');
const { spawn } = require('child_process');

// Replace with the GitLab branch you want to push to
const BRANCH_NAME = 'main';


const getCurrentDateTime = () => {
  const date = new Date();
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const year = date.getFullYear().toString();
  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');
  const formattedDate = `${day}-${month}-${year}`;
  const formattedTime = `${hours}-${minutes}`;
  return `${formattedDate}_${formattedTime}`;
}

const commitToGitLab = (branchName, commitMessage, mergeRequestTitle) => {
  try {
    // Set Git user details
    //execSync('git config --global user.email "you@example.com"');
    //execSync('git config --global user.name "Your Name"');

    // Add GitLab remote and fetch changes
    //execSync(`git remote add origin ${REPO_URL}`);
    execSync(`git fetch origin ${BRANCH_NAME}`);

    execSync(`git checkout -b ${branchName}`);
    execSync('git add .');
    execSync(`git commit -m "${commitMessage}"`);
    execSync(`git push --set-upstream origin ${branchName} --force-with-lease=refs/heads/${BRANCH_NAME} -o merge_request.create -o merge_request.title="${mergeRequestTitle}"`)
    console.log('Changes committed to GitLab successfully!');
  } catch (error) {
    console.error('Error committing chang es to GitLab:', error);
  }
}

const createBranchAndCommitChanges = async (branchName, commitMessage, mergeRequestTitle) => {
  const gitStatus = spawn('git', ['status']);

  gitStatus.stdout.on('data', async (data) => {
    const output = data.toString();
    if (output.includes('Changes not staged for commit') || output.includes('Changes to be committed') || output.includes('Untracked files')) {
      console.log('There are changes in the local repository.');
      commitToGitLab(branchName, commitMessage, mergeRequestTitle);
      //`test commit : ${Date.now()}`, `New-Branch-${getCurrentDateTime()}`
    } else {
      console.log('There are no changes in the local repository.');
    }
  });

  gitStatus.stderr.on('data', (data) => {
    console.error(`Error: ${data}`);
  });
};

module.exports = createBranchAndCommitChanges;